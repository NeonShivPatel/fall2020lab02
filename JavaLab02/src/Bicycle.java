//@Author: Shiv Patel
//ID: 1935098
public class Bicycle {
	private String manufacturer; 
	private int numberGears;
	private double maxSpeed;
	public Bicycle (String a, int b, double c) {
		manufacturer = a;
		numberGears = b;
		maxSpeed = c;
	}
	
	public String getManufacturer() {
		return manufacturer;
	}
	
	public int getNumberGears() {
		return numberGears;
		
	}
	
	public double getMaxSpeed() {
		return maxSpeed;
		
	}
	
	public String toString() {
		return "Manufaturer: " + this.manufacturer + ", The Number of Gears: " + this.numberGears +  ", MaxSpeed: " + this.maxSpeed;
	}
}
