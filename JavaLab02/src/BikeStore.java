//@Author: Shiv Patel
//ID: 1935098
public class BikeStore {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Bicycle[] gang = new Bicycle[4];
		gang[0] = new Bicycle("Raleigh", 18, 34);
		gang[1] = new Bicycle("Focus", 21, 40);
		gang[2] = new Bicycle("Felt", 24, 46);
		gang[3] = new Bicycle("Specialized", 26, 51);
		
		for (int i=0; i < gang.length; i++) {
			System.out.println(gang[i].toString());
		}
		
	}

}
